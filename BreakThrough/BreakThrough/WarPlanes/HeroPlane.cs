﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakThrough.Warplanes
{
    abstract class HeroPlane : Warplane
    {
        protected virtual Arms.Weapons.Weapon SubWeapon { get; set; }

        public abstract void ChangeWeapon();
    }
}
