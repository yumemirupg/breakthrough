﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BreakThrough.Arms.Armors
{
    abstract class Armor
    {
        public short durability { get; protected set; }
        public byte hardness { get; protected set; }
        public byte weight { get; protected set; }
        public bool stealth { get; protected set; }
    }
}
