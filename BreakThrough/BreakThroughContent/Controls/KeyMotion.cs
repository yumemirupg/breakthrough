﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Controls
{
    public class KeyMotion : InputMotion
    {
        private Keys key;

        internal KeyMotion(int keyValue)
        {
            key = (Keys)keyValue;
        }

        public override bool IsOn
        {
            get
            {
                if (InputLoader.KeyboardLoader.NewKeys[key] == KeyState.Down)
                    return true;
                return false;
            }
        }

        public override bool IsJustOn
        {
            get
            {
                if (InputLoader.KeyboardLoader.NewKeys[key] == KeyState.Down
                    && InputLoader.KeyboardLoader.OldKeys[key] == KeyState.Up)
                    return true;
                return false;
            }
        }

        public override bool IsJustOff
        {
            get
            {
                if (InputLoader.KeyboardLoader.NewKeys[key] == KeyState.Up
                   && InputLoader.KeyboardLoader.OldKeys[key] == KeyState.Down)
                    return true;
                return false;
            }
        }
    }
}
