﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Controls
{
    public class MouseInputLoader
    {
        internal MouseState NewMouse;
        internal MouseState OldMouse;
        internal bool[] NewMouseButton = new bool[3];
        internal bool[] OldMouseButton = new bool[3];
        public MouseClickMotion LeftClick;
        public MouseClickMotion MiddleClick;
        public MouseClickMotion RightClick;
        public PositionMotion Position;

        internal MouseInputLoader()
        {
            NewMouse = Mouse.GetState();
            OldMouse = NewMouse;

            LeftClick = new MouseClickMotion(0);
            MiddleClick = new MouseClickMotion(1);
            RightClick = new MouseClickMotion(2);
            Position = new PositionMotion();
        }

        internal void GetInput()
        {
           OldMouse = NewMouse;
           NewMouse = Mouse.GetState();

           OldMouseButton[0] = (OldMouse.LeftButton == (ButtonState)1);
           OldMouseButton[1] = (OldMouse.MiddleButton == (ButtonState)1);
           OldMouseButton[2] = (OldMouse.RightButton == (ButtonState)1);
           NewMouseButton[0] = (NewMouse.LeftButton == (ButtonState)1);
           NewMouseButton[1] = (NewMouse.MiddleButton == (ButtonState)1);
           NewMouseButton[2] = (NewMouse.RightButton == (ButtonState)1);
        }

    }
}
