﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Controls
{
    public class InputLoader
    {
        public static MouseInputLoader MouseLoader;
        
        public static KeyboardInputLoader KeyboardLoader;

        static InputLoader() 
        {
            MouseLoader = new MouseInputLoader();

            KeyboardLoader = new KeyboardInputLoader();
        }

        void GetInput() 
        {
            MouseLoader.GetInput();
            KeyboardLoader.GetInput();
        }
    }
}
