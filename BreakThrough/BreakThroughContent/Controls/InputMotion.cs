﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Controls
{
    public abstract class InputMotion
    {
        public abstract bool IsOn { get; }
        public abstract bool IsJustOn { get; }
        public abstract bool IsJustOff { get; }
    }
}
